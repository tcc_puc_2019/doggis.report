from typing import List

from infraestructure.repository.dashboard import DashboardORM


class ReportService:

    def __init__(self, dashboard_orm: DashboardORM):
        self.dashboard_orm = dashboard_orm

    def retrieve_report(self, filter_arguments) -> List:
        return self.dashboard_orm.retrieve_report(filter_arguments)