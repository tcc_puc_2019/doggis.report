import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

requirements = ['pytest-xdist', 'paramiko', 'flask', 'flask-restplus', 'pymongo', 'apscheduler', 'requests', 'mockupdb', 'flask-eureka', 'gunicorn']

setuptools.setup(
    name="eat-dashboard",
    version="0.0.1",
    author="Gustavo Lopes Barbosa da Silva",
    author_email="gustavo.silval@sonda.com",
    description="Dashboard backend to show tests results of all Embraer systems.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=requirements,
    packages=setuptools.find_packages(),
    classifiers=[
        "Development Status :: 1 - Planning",
        "Programming Language :: Python :: 3.7",
        "License :: Other/Proprietary License",
        "Framework :: Flask",
        "Operating System :: POSIX :: Linux",
    ],

)
