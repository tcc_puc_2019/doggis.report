from flask_restplus import fields
from configuration.api_handler import api


report_model = api.model('Report Data', {
    'paymentType': fields.String(attribute='payment_type'),
    'petName': fields.String(attribute='pet_name'),
    'petRace': fields.String(attribute='pet_race'),
    'serviceName': fields.String(attribute='service_name'),
    'serviceType': fields.String(attribute='service_type'),
    'professionalName': fields.String(attribute='professional_name'),
    'clientName': fields.String(attribute='client_name'),
    'executionDate': fields.Integer(attribute='execution_date'),
    'value': fields.Float(attribute='value'),
    'rate': fields.Integer(attribute='rate'),
    'generatedPataz': fields.Integer(attribute='generated_pataz'),
})
