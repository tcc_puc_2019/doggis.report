class ShopData:

    payment_type: str
    pet_name: str
    pet_race: str
    service_name: str
    service_type: str
    professional_name: str
    client_name: str
    execution_date: int
    value: float
    rate: int
    generated_pataz: int

    def __init__(self, data: dict):
        self.payment_type = data['paymentType']
        self.pet_name = data['petName']
        self.pet_race = data['petRace']
        self.service_name = data['serviceName']
        self.service_type = data['serviceType']
        self.professional_name = data['professionalName']
        self.client_name = data['clientName']
        self.execution_date = data['executionDate']
        self.value = data['value']
        self.rate = data['rate']
        self.generated_pataz = data['generatedPataz']