from flask_restplus import reqparse

filter_arguments = reqparse.RequestParser()
filter_arguments.add_argument('paymentType', type=str, required=False, help='Filtered Payment Type')
filter_arguments.add_argument('petName', type=str, required=False, help='Filtered Pet Name')
filter_arguments.add_argument('petRace', type=str, required=False, help='Filtered Pet Race')
filter_arguments.add_argument('serviceName', type=str, required=False, help='Filtered Service Name')
filter_arguments.add_argument('serviceType', type=str, required=False, help='Filtered Service Type')
filter_arguments.add_argument('professionalName', type=str, required=False, help='Filtered Professional Name')
filter_arguments.add_argument('clientName', type=str, required=False, help='Filtered Client Name')
filter_arguments.add_argument('value', type=float, required=False, help='Filtered Value')
filter_arguments.add_argument('rate', type=int, required=False, help='Filtered Rate')
filter_arguments.add_argument('generatedPataz', type=int, required=False, help='Filtered Generated Pataz')