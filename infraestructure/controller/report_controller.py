import logging

from acl.report_model import report_model
from configuration.api_handler import api
from flask import current_app
from flask_restplus import Resource

from infraestructure.controller.parse_arguments.filter_arguments import filter_arguments

log = logging.getLogger(__name__)

ns = api.namespace('report', description='Report')


@ns.route('/')
class EATReportAPI(Resource):

    @api.expect(filter_arguments)
    @api.marshal_list_with(report_model)
    def get(self):
        return current_app.config['REPORT_SERVICE'].retrieve_report(filter_arguments)
