import json
import os
from datetime import datetime, timedelta

from flask import request, current_app
from mockupdb import go
from pymongo import MongoClient

from domain.shop_data.shop_data import ShopData


class DashboardORM:
    client: MongoClient

    def __init__(self, client: MongoClient):
        self.client = client

    def retrieve_report(self, filter_arguments):
        db = self.client[current_app.config["DB"]]
        if current_app.config["DB_PASS"] != '':
            db.authenticate(current_app.config["DB_USER"], current_app.config["DB_PASS"])
        filters = dict()
        for filter in filter_arguments.parse_args(request):
            if filter_arguments.parse_args(request).get(filter):
                filters[filter] = filter_arguments.parse_args(request).get(filter)
        cursor = db.shop_data.find(filters)
        shop_data_list = []
        for data in cursor:
            shop_data_list.append(ShopData(data))

        return shop_data_list

    def create_database_initial(self):
        db = self.client[current_app.config["DB"]]
        if current_app.config["DB_PASS"] != '':
            db.authenticate(current_app.config["DB_USER"], current_app.config["DB_PASS"])
        db.shop_data.remove({})
        base_data = 'base_data'
        with open(os.path.join(os.getcwd(), base_data, 'shop_data.json')) as json_file:
            file_data = json.load(json_file)
        db.shop_data.insert(file_data)
