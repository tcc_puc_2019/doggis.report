import os
import re

from cryptography.fernet import Fernet
from pymongo import MongoClient

from application.report_service import ReportService
from infraestructure.repository.dashboard import DashboardORM

class Config:
    mongo_url_search = re.search(r'(mongodb\:\/\/)(.*)(\:)(.*)(\@)(.*)(\:)(\d+)(\/)(.*)', os.environ.get('MONGODB_URI'))
    MONGO_CLIENT = MongoClient(mongo_url_search.group(1) +
                               mongo_url_search.group(6) +
                               mongo_url_search.group(7) +
                               mongo_url_search.group(8) +
                               mongo_url_search.group(9) +
                               mongo_url_search.group(10) +
                               "?retryWrites=false")
    DB_USER = mongo_url_search.group(2)
    DB_PASS = mongo_url_search.group(4)
    DB = mongo_url_search.group(10)
    DASHBOARD_ORM = DashboardORM(MONGO_CLIENT)
    REPORT_SERVICE = ReportService(DASHBOARD_ORM)
    SERVER_NAME = "localhost:" + os.environ.get('PORT', '5000')
    DEBUG = False
    TESTING = False
    CIPHER_SUITE = Fernet(b'_WP9GVkaFdhjLXDbGW-5XHtPvz5L6GUInzecUx6imow=')


class TestingConfig(Config):
    DEBUG = True
    TESTING = True

