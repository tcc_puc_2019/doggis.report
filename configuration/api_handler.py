import logging
from flask_restplus import Api

log = logging.getLogger('dashboard')

api = Api(version='1.0', title='Dashboard API',
          description='All Doggis Shop results.')


@api.errorhandler
def default_error_handler(e):
    message = 'An unhandled exception occurred.'
    log.exception(message)
    return {'message': message}, 500
