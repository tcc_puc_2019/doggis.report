from enum import IntEnum


class TestTypeEnum(IntEnum):
    FUNCTIONAL = 1
    HEALTH = 2

