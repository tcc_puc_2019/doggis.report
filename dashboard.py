import os
import logging.config
from infraestructure.controller.report_controller import ns as report_namespace

from flask import Flask, Blueprint

from configuration.app_config import Config, TestingConfig
from configuration.api_handler import api

logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), 'logging.conf'))
logging.config.fileConfig(logging_conf_path)
log = logging.getLogger('dashboard')

def start_database(app):
    app.config['DASHBOARD_ORM'].create_database_initial()

def configure_app(app, profile):
    app.config.update({key: value for key, value in Config.__dict__.items() if '__' not in key})
    if profile == 'test':
        app.config.update({key: value for key, value in TestingConfig.__dict__.items() if '__' not in key})


def init_app(profile='local'):
    app = Flask('dashboard')
    with app.app_context():
        configure_app(app, profile)
    blueprint = Blueprint('api', 'dashboard')
    api.init_app(blueprint)
    api.add_namespace(report_namespace)
    app.register_blueprint(blueprint)
    with app.app_context():
        start_database(app)
    return app


if __name__ == '__main__':
    app = init_app()
    log.info(f">>>>> Starting development server at http://{app.config['SERVER_NAME']}/ <<<<<")
    app.run(threaded=True, debug=True, host='0.0.0.0', port=os.environ.get('PORT', 5000))