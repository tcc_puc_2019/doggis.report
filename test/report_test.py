
import pytest

from test.fixture_creation import read_json
from .fixture_creation import client, app


@pytest.mark.usefixtures(app.__name__, client.__name__)
def test_full_report(client):
    """A test runner for the applications commands."""
    response = client.get('report/')
    assert response.status_code == 200
    assert response.json == read_json('input/shop_data.json')


@pytest.mark.usefixtures(app.__name__, client.__name__)
def test_name_filtered_report(client):
    """A test runner for the applications commands."""
    response = client.get('report/?clientName=Gustavo')
    assert response.status_code == 200
    assert response.json == read_json('input/gustavo_shop_data.json')