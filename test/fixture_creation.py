import json
import os
from typing import List
import pytest
from dashboard import init_app


@pytest.fixture
def app():
    test_app = init_app('test')
    client = test_app.test_client()
    yield test_app

@pytest.fixture
def client(app):
    """A test client for the app."""
    return app.test_client()

@pytest.fixture
def runner(app):
    """A test runner for the app's Click commands."""
    return app.test_cli_runner()


def read_json(path: str) -> List:
    with open(os.path.join(os.getcwd(), 'test', path)) as json_file:
        return json.load(json_file)
